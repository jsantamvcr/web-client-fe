/**
 * Constantes para centralizar los mensajes
 * personalizados
 */
const customMessage = {
    /**Existe un error, validar con el admin del sistema */
    DEFAULT_ERROR: 'Existe un error, validar con el admin del sistema',
    /**Error server */
    ERROR_SERVER: 'Error en el servidor',
    /**El Nombre, apellidos y email son obligatorios*/
    REQ_NAME_LASTNAME: 'El Nombre, apellidos y email son obligatorios',
    /**Usuario actualizado correctamente */
    UPDATE_USER: 'Usuario actualizado correctamente',
    /**Todos los campos son requeridos */
    ALL_FIELD_REQ: 'Todos los campos son requeridos',
    /**Datos almacenados correctamente */
    DATA_SAVE_SUCCESS: 'Datos almacenados correctamente',
    /**Loading data */
    LOADINDG: 'Cargando...'
}

/**
 * Tipos de status para los errores
 * y caso correctos
 */
const typeStatus = {
    error: 'error',
    success: 'success',
    warning: 'warning',
    danger: 'danger'

}

export {
    customMessage,
    typeStatus
}