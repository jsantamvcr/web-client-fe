import { LayoutAdmin } from "../layouts/LayoutAdmin";
import { LayoutBasic } from "../layouts/LayoutBasic";

//Admin Pages
import AdminBlog from '../pages/Admin/Blog'
import {
    Admin as AdminHome,
    MenuWeb as AdminMenuWeb,
    SingIn as AdminSingIn,
    Users as AdminUsers
} from '../pages/Admin'

//Public Pages
import {
    Home,
    Contact,
    Error404,
    Courses,
    AdminCourses,
    Blog
} from '../pages'

const routes = [
    {
        path: "/admin",
        component: LayoutAdmin,
        exact: false,
        routes: [
            {
                path: '/admin',
                component: AdminHome,
                exact: true
            },
            {
                path: '/admin/login',
                component: AdminSingIn,
                exact: true
            },
            {
                path: '/admin/users',
                component: AdminUsers,
                exact: true
            },
            {
                path: '/admin/menu',
                component: AdminMenuWeb,
                exact: true
            },
            {
                path: '/admin/courses',
                component: AdminCourses,
                exact: true
            },
            {
                path: '/admin/blog',
                component: AdminBlog,
                exact: true
            },
            {
                component: Error404,
            }
        ]
    },
    {
        path: '/',
        component: LayoutBasic,
        exact: false,
        routes: [
            {
                path: '/',
                component: Home,
                exact: true
            },
            {
                path: '/contact',
                component: Contact,
                exact: true
            },
            {
                path: '/courses',
                component: Courses,
                exact: true
            },
            {
                path: '/blog',
                component: Blog,
                exact: true
            },
            {//Level for dinamic blocs
                path: '/blog/:url',
                component: Blog,
                exact: true
            },            
            {
                component: Error404,
            }
        ]
    }
]

export default routes