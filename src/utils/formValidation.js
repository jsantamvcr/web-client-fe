/**
 * Validamos el maximo del formulario
 * @param {*} inputData 
 * @param {*} minLength 
 * @returns 
 */
export const minLength = (inputData, minLength = 6) => {
    const { value } = inputData

    removeClassErrorSucces(inputData)

    if (value.length >= minLength) {
        inputData.classList.add('success')
        return true
    } else {
        inputData.classList.add('error')
        return false
    }
}


// export const emailValid = (inputData) => {

//     const emailValid = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//     const { value } = inputData

//     removeClassErrorSucces(inputData)

//     const resultValidation = emailValid.test(value)
//     if (resultValidation) {
//         inputData.classList.add('success')
//         return true
//     } else {
//         inputData.classList.add('error')
//         return false
//     }
// }


const removeClassErrorSucces = (inputData) => {
    inputData.classList.remove('success')
    inputData.classList.remove('error')
}




