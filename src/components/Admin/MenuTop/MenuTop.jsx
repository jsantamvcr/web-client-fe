import React from 'react'
import { Button } from 'antd'
import { MenuFoldOutlined, MenuUnfoldOutlined, PoweroffOutlined } from '@ant-design/icons'
import LogoMenuTop from '../../../assets/images/png/logo-white.png'
import { logout } from '../../../api/auth.api'
import './MenuTop.scss'

export const MenuTop = (props) => {

    const { menuCollapsed, setMenuCollapsed } = props

    const logOutUser = () => {
        logout()
        window.location.reload()
    }

    return (
        <>
            <div className='menu-top'>
                <div className='menu-top__left'>
                    <img className='menu-top__left-log'
                        src={LogoMenuTop}
                        alt="Juan Carlos Santamaria"
                    />

                    <Button type='link' onClick={() => setMenuCollapsed(!menuCollapsed)}>
                        {!menuCollapsed ? <MenuFoldOutlined /> : <MenuUnfoldOutlined />}
                    </Button>
                </div>

                <div className='menu-top__right'>
                    <Button type='link' onClick={logOutUser}>
                        <PoweroffOutlined />
                    </Button>
                </div>

            </div>
        </>
    )
}
