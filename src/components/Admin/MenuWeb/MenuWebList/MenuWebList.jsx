import './MenuWebList.scss'
import React, { useEffect, useState } from 'react'
import { Switch, List, Button, notification, Modal as ModalAntd } from 'antd'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import DragSortableList from 'react-drag-sortable'
import { updateMenuApi, deleteMenuApi } from '../../../../api/menu'
import { Modal } from '../../../Modal'
import { getAccessToken } from '../../../../api/auth.api'
import { AddMenuWeb } from '../AddMenuWeb/AddMenuWeb'
import { EditMenuWeb } from '../EditMenuWeb/EditMenuWeb'
import { customMessage as msg, typeStatus as ts } from '../../../../constants/customMessage'

const { confirm } = ModalAntd

export const MenuWebList = (props) => {
    const { menu, setReloadMenuWeb } = props
    const [listItems, setListItems] = useState([])
    const [isVisibleModal, setIsVisibleModal] = useState(false)
    const [modalTitle, setModalTitle] = useState('')
    const [modalContent, setModalContent] = useState(null)

    useEffect(() => {
        const listIemsArr = []
        menu.forEach(item => {
            listIemsArr.push({
                content: (
                    <MenuItem
                        item={item}
                        activateMenu={activateMenu}
                        editMenuWebModal={editMenuWebModal}
                        deleteMenu={deleteMenu}
                    />
                )
            })
        });
        setListItems(listIemsArr)
    }, [menu])

    //activo o desactivo los menos
    const activateMenu = (menu, active) => {
        const token = getAccessToken()

        updateMenuApi(token, menu._id, { active })
            .then(response => {
                notification[ts.success]({
                    message: response.msg
                })
            })
    }

    //Ordeno los menus
    const onSort = (sortedList, dropEvent) => {
        const token = getAccessToken()
        sortedList.forEach(item => {
            const { _id } = item.content.props.item
            const order = item.rank
            updateMenuApi(token, _id, { order })
        })
    }

    //Eliminar los Menu
    const deleteMenu = (menu) => {
        const token = getAccessToken()
        confirm({
            title: 'Eliminando menu',
            content: `Seguro de eliminar el menu: [ ${menu.title} ]?`,
            okText: 'Eliminar',
            okType: 'danger',
            cancelText: 'Cancelar',
            onOk() {
                deleteMenuApi(token, menu._id)
                    .then(response => {
                        notification[ts.success]({
                            message: response.msg
                        })
                        setReloadMenuWeb(true)
                    })
                    .catch(() => {
                        notification[ts.error]({
                            message: msg.ERROR_SERVER
                        })
                    })
            }
        })
    }

    const addMenuWebModal = () => {
        setIsVisibleModal(true)
        setModalTitle("Creando nuevo menu")
        setModalContent(
            <AddMenuWeb
                setIsVisibleModal={setIsVisibleModal}
                setReloadMenuWeb={setReloadMenuWeb}
            />

        )
    }

    const editMenuWebModal = menu => {
        setIsVisibleModal(true)
        setModalTitle(`Editando Menu: ${menu.title}`)
        setModalContent(
            <EditMenuWeb
                setIsVisibleModal={setIsVisibleModal}
                setReloadMenuWeb={setReloadMenuWeb}
                menu={menu}
            />
        )
    }

    return (
        <div className='menu-web-list'>
            <div className='menu-web-list__header'>
                <Button type='primary' onClick={addMenuWebModal}>
                    Crear Menú
                </Button>
            </div>
            <div className='menu-web-list__items'>
                <DragSortableList items={listItems} onSort={onSort} type='vertical' />
            </div>

            <Modal
                title={modalTitle}
                isVisible={isVisibleModal}
                setIsVisible={setIsVisibleModal}
            >
                {modalContent}
            </Modal>
        </div>
    )
}

const MenuItem = (props) => {
    const { item, activateMenu, editMenuWebModal, deleteMenu } = props

    return (
        <List.Item
            actions={[
                <Switch defaultChecked={item.active} onChange={e => activateMenu(item, e)} />,
                <Button type='primary' onClick={() => editMenuWebModal(item)}>
                    <EditOutlined />
                </Button>,
                <Button type='danger' onClick={e => deleteMenu(item)}>
                    <DeleteOutlined />
                </Button>
            ]}>
            <List.Item.Meta title={item.title} description={item.url} />
        </List.Item>
    )
}
