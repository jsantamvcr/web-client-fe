import React, { useEffect, useState } from 'react'
import { Form, Input, Button, notification } from 'antd'
import { LinkOutlined, MenuOutlined } from '@ant-design/icons'
import { updateMenuApi, getAccessToken } from '../../../../api'
import { customMessage as msg, typeStatus as ts } from '../../../../constants/customMessage'

import './EditMenuWeb.scss'

export const EditMenuWeb = (props) => {
    const { setIsVisibleModal, setReloadMenuWeb, menu } = props
    
    const [menuWebData, setMenuWebData] = useState(menu)

    useEffect(() => {
        setMenuWebData(menu)
    }, [menu])

    const editMenu = e => {
        e.preventDefault()

        if (!menuWebData.title || !menuWebData.url) {
            notification[ts.error]({
                message: msg.ALL_FIELD_REQ
            })
        } else {
            const token = getAccessToken()
            updateMenuApi(token, menuWebData._id, menuWebData)
                .then(response => {
                    notification[ts.success]({
                        message: msg.DATA_SAVE_SUCCESS
                    })
                    setIsVisibleModal(false)
                    setReloadMenuWeb(true)
                })
                .catch(() => {
                    notification[ts.error]({                        
                        message: msg.ERROR_SERVER
                    })
                })
        }
    }

    return (
        <>
            <h1>{menu.title}</h1>
            <EditForm
                menuWebData={menuWebData}
                setMenuWebData={setMenuWebData}
                editMenu={editMenu}
            />
        </>
    )
}



const EditForm = (props) => {
    const { menuWebData, setMenuWebData, editMenu } = props

    return (
        <Form className='form-add'
            onSubmitCapture={editMenu}
        >
            <Form.Item>
                <Input
                    prefix={<MenuOutlined />}
                    placeholder='Titulo'
                    value={menuWebData.title}
                    onChange={e => setMenuWebData({ ...menuWebData, title: e.target.value })}
                />
            </Form.Item>

            <Form.Item>
                <Input
                    prefix={<LinkOutlined />}
                    placeholder='url'
                    value={menuWebData.url}
                    onChange={e => setMenuWebData({ ...menuWebData, url: e.target.value })}
                />
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType='submit' className='btn-submit' >
                    Editar menu
                </Button>
            </Form.Item>
        </Form>
    )
}