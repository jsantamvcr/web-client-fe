import React, { useState } from "react";
import { Form, Input, Button, Select, notification } from "antd";
import { LinkOutlined, MenuOutlined } from "@ant-design/icons";
import { addMenuApi, getAccessToken } from "../../../../api";

import "./AddMenuWeb.scss";
import {
  typeStatus as ts,
  customMessage as msg,
} from "../../../../constants/customMessage";

/**
 * Formulario para creacion de menus
 * @param {props} props
 * @returns
 */
export const AddMenuWeb = (props) => {
  const { setIsVisibleModal, setReloadMenuWeb } = props;
  const [menuWebData, setMenuWebData] = useState({});

  const addMenu = (e) => {
    e.preventDefault();
    let finalData = {
      title: menuWebData.title,
      url: (menuWebData.http ? menuWebData.http : "http://") + menuWebData.url,
    };

    if (!finalData.title || !finalData.url || !menuWebData.url) {
      notification[ts.error]({
        message: msg.ALL_FIELD_REQ,
      });
    } else {
      const token = getAccessToken();
      finalData.active = true;
      finalData.order = 1000;
      addMenuApi(token, finalData)
        .then((response) => {
          notification[ts.success]({
            message: msg.DATA_SAVE_SUCCESS,
          });
          setIsVisibleModal(false);
          setReloadMenuWeb(true);
          setMenuWebData({});
          finalData = {};
          console.log(response);
        })
        .catch(() => {
          notification[ts.error]({
            message: msg.ERROR_SERVER,
          });
        });
    }
  };

  return (
    <div className="add-menu-web-form">
      <AddForm
        menuWebData={menuWebData}
        setMenuWebData={setMenuWebData}
        addMenu={addMenu}
      />
    </div>
  );
};

/**
 * Componente interno para la creacion ddel
 * formulario
 * @param {props} props
 * @returns Formulario
 */
const AddForm = (props) => {
  const { menuWebData, setMenuWebData, addMenu } = props;
  const { Option } = Select;

  const selectBefore = (
    <Select
      defaultValue="http://"
      style={{ width: 90 }}
      onChange={(e) => setMenuWebData({ ...menuWebData, http: e })}
    >
      <Option value="http://">http://</Option>
      <Option value="https://">https://</Option>
    </Select>
  );

  return (
    <Form className="form-add" onSubmitCapture={addMenu}>
      <Form.Item>
        <Input
          prefix={<MenuOutlined />}
          placeholder="Titulo"
          value={menuWebData.title}
          onChange={(e) =>
            setMenuWebData({ ...menuWebData, title: e.target.value })
          }
        />
      </Form.Item>

      <Form.Item>
        <Input
          prefix={<LinkOutlined />}
          addonBefore={selectBefore}
          placeholder="url"
          value={menuWebData.url}
          onChange={(e) =>
            setMenuWebData({ ...menuWebData, url: e.target.value })
          }
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" className="btn-submit">
          Crear menu
        </Button>
      </Form.Item>
    </Form>
  );
};
