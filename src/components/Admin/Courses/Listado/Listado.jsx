import React, { useEffect, useState } from "react";
import { List, Button, Modal as ModalAntd, notification } from "antd";
import {
  AppstoreAddOutlined,
  DeleteFilled,
  EditFilled,
} from "@ant-design/icons";
import DragSortableList from "react-drag-sortable";
import { Modal } from "../../../Modal";
import { AddEditCourse } from "../AddEditCourse/AddEditCourse";
import {
  deleteCourseApi,
  getAccessToken,
  getUdemyCoursesApi,
  updateCourse,
} from "../../../../api";
import {
  customMessage as msg,
  typeStatus as ts,
} from "../../../../constants/customMessage";
import "./Listado.scss";

const { confirm } = ModalAntd;

export const Listado = (props) => {
  const { courses, setReloadCourses } = props;
  const [listCourses, setListCourses] = useState([]);
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalContent, setModalContent] = useState(null);

  useEffect(() => {
    const listCoursesArray = [];
    courses.forEach((course) => {
      listCoursesArray.push({
        content: (
          <Courses
            course={course}
            deleteCourse={deleteCourse}
            editCoursesModal={editCoursesModal}
          />
        ),
      });
    });
    setListCourses(listCoursesArray);
  }, [courses]);

  const onSort = (sortedList, dropEvent) => {
    const token = getAccessToken();
    sortedList.forEach((item) => {
      const { uid } = item.content.props.course;
      const order = item.rank;
      updateCourse(token, { uid, order })
       
    });
  };

  const addEditCoursesModal = () => {
    setIsVisibleModal(true);
    setModalTitle("Creando nuevo curso");
    setModalContent(
      <AddEditCourse
        setIsVisibleModal={setIsVisibleModal}
        setReloadCourses={setReloadCourses}
      />
    );
  };

  /**
   * Edtiamos un nuevo curso
   */
  const editCoursesModal = (course) => {
    setIsVisibleModal(true);
    setModalTitle("Edtitar curso");
    setModalContent(
      <AddEditCourse
        setIsVisibleModal={setIsVisibleModal}
        setReloadCourses={setReloadCourses}
        course={course}
      />
    );
  };

  const deleteCourse = (course) => {
    const token = getAccessToken();

    confirm({
      title: "Eliminado Curso",
      content: `Seguro de eliminar el curso ${course.idCourse}`,
      okText: "Elminar",
      okType: "danger",
      cancelText: "Cancelar",
      onOk() {
        deleteCourseApi(token, course.uid)
          .then((response) => {
            const typeNotification =
              response.status === 200 ? ts.success : ts.warning;
            notification[typeNotification]({
              message: response.data.msg,
            });
            setReloadCourses(true);
          })
          .catch(() => {
            notification[ts.error]({
              message: msg.ERROR_SERVER,
            });
          });
      },
    });
  };

  return (
    <div className="courses-list">
      <div className="courses-list__header">
        <Button type="primary" onClick={addEditCoursesModal}>
          <AppstoreAddOutlined /> Nuevo curso
        </Button>
      </div>

      <div className="courses-list__items">
        {listCourses.length === 0 && (
          <h2 style={{ textAlign: "center", margin: 0 }}>
            No tiene cursos asignados.
          </h2>
        )}

        <DragSortableList items={listCourses} onSort={onSort} type="vertical" />

        <Modal
          title={modalTitle}
          isVisible={isVisibleModal}
          setIsVisible={setIsVisibleModal}
        >
          {modalContent}
        </Modal>
      </div>
    </div>
  );
};

/**
 * Componente para pintar
 * los cursos disponi
 * @param {Object} props
 * @returns
 */
const Courses = (props) => {
  const { course, deleteCourse, editCoursesModal } = props;
  const [courseData, setCourseData] = useState(null);

  useEffect(() => {
    getUdemyCoursesApi(course.idCourse).then((response) => {
      if (response.status !== 200) {
        notification[ts.warning]({
          message: `El curso con el id ${course.idCourse} no existe`,
        });
      }
      setCourseData(response.data);
    });
  }, [course]);

  if (!courseData) return null;

  return (
    <List.Item
      actions={[
        <Button type="primary" onClick={() => editCoursesModal(course)}>
          <EditFilled />
        </Button>,
        <Button type="danger" onClick={() => deleteCourse(course)}>
          <DeleteFilled />
        </Button>,
      ]}
    >
      <img
        src={courseData.image_480x270}
        alt={courseData.title}
        style={{ width: "100px", marginRight: "20px" }}
      />
      <List.Item.Meta
        title={`ID: ${course.idCourse} - ${courseData.title}`}
        description={courseData.headline}
      />
    </List.Item>
  );
};
