import React, { useEffect, useState } from "react";
import { Form, Input, Button, notification } from "antd";
import { getAccessToken, addCourse, updateCourse } from "../../../../api";
import {
  DollarOutlined,
  GiftFilled,
  KeyOutlined,
  AppstoreAddOutlined,
  LinkOutlined,
} from "@ant-design/icons";
import {
  customMessage as msg,
  typeStatus as ts,
} from "../../../../constants/customMessage";

import "./AddEditCourse.scss";

export const AddEditCourse = (props) => {
  const { setIsVisibleModal, setReloadCourses, course } = props;
  const [courseData, setCourseData] = useState({});

  useEffect(() => {
    course ? setCourseData(course) : setCourseData({});
  }, [course]);

  const addCourseApi = (e) => {
    e.preventDefault();

    const token = getAccessToken();

    addCourse(token, courseData)
      .then((response) => {
        const notif = response.status === 200 ? ts.success : ts.warning;
        notification[notif]({
          message: response.data.msg,
        });
        setReloadCourses(true);
        setIsVisibleModal(false);
        setCourseData({});
      })
      .catch(() => {
        notification[ts.error]({
          message: msg.ERROR_SERVER,
        });
      });
  };

  const updateCourseApi = (e) => {
    e.preventDefault();
    const token = getAccessToken();

    updateCourse(token, courseData)
      .then((response) => {
        const notif = response.status === 200 ? ts.success : ts.warning;
        notification[notif]({
          message: response.data.msg,
        });
        setReloadCourses(true);
        setIsVisibleModal(false);
        setCourseData({});
      })
      .catch(() => {
        notification[ts.error]({
          message: msg.ERROR_SERVER,
        });
      });
  };

  return (
    <div className="add-edit-courses-form">
      <AddEditForm
        course={course}
        addCourseApi={addCourseApi}
        updateCourseApi={updateCourseApi}
        setCourseData={setCourseData}
        courseData={courseData}
      />
    </div>
  );
};

const AddEditForm = (props) => {
  const { course, updateCourseApi, addCourseApi, setCourseData, courseData } =
    props;

  return (
    <Form
      className="form-add-edit"
      onSubmitCapture={course ? updateCourseApi : addCourseApi}
    >
      <Form.Item>
        <Input
          prefix={<KeyOutlined />}
          placeholder="Id del curso"
          value={courseData.idCourse}
          onChange={(e) =>
            setCourseData({ ...courseData, idCourse: e.target.value })
          }
          disabled={course ? true : false}
        />

        <Input
          prefix={<DollarOutlined />}
          placeholder="Precio del curso"
          value={courseData.price}
          onChange={(e) =>
            setCourseData({ ...courseData, price: e.target.value })
          }
        />

        <Input
          prefix={<LinkOutlined />}
          placeholder="Url del Curso"
          value={courseData.link}
          onChange={(e) =>
            setCourseData({ ...courseData, link: e.target.value })
          }
        />

        <Input
          prefix={<GiftFilled />}
          placeholder="Cupon de descuento"
          value={courseData.coupon}
          onChange={(e) =>
            setCourseData({ ...courseData, coupon: e.target.value })
          }
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="btn-submit">
          <AppstoreAddOutlined />
          {course ? "Actualizar curso" : "Crear curso"}
        </Button>
      </Form.Item>
    </Form>
  );
};
