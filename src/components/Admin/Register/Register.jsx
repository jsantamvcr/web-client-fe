import React, { useState } from "react";
import { Form, Input, Button, Checkbox, notification } from "antd";
import {
  LockOutlined,
  UserAddOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { minLength } from "../../../utils/formValidation";
import { signUpApi } from "../../../api/user.api";

import "./Register.scss";

export const Register = () => {
  const [input, setInput] = useState({
    correo: "",
    password: "",
    repeatPassword: "",
    privacyPolicy: false,
  });

  const [formValid, setFormValid] = useState({
    correo: false,
    password: false,
    repeatPassword: false,
    privacyPolicy: false,
  });

  const changeForm = (e) => {
    if (e.target.name === "privacyPolicy") {
      setInput({
        ...input,
        [e.target.name]: e.target.checked,
      });
    } else {
      setInput({
        ...input,
        [e.target.name]: e.target.value,
      });
    }
  };

  const inputValidation = (e) => {
    const { type, name } = e.target;

    // if (type === 'correo') {
    //     setFormValid({ ...formValid, [name]: emailValid(e.target) })
    // }

    if (type === "password") {
      setFormValid({ ...formValid, [name]: minLength(e.target) });
    }

    if (type === "checkbox") {
      setFormValid({ ...formValid, [name]: e.target.checked });
    }
  };

  const register = async (e) => {
    e.preventDefault();

    const {
      correo: emailVal,
      password: passwordVal,
      repeatPassword: repeatPasswordVal,
      privacyPolicy: privacyPolicyVal,
    } = input;
    //const { email, password, repeatPassword, privacyPolicy } = formValid

    if (!emailVal || !passwordVal || !repeatPasswordVal || !privacyPolicyVal) {
      notification["error"]({
        message: "Todos los campos son obligatorios",
      });
    } else {
      if (passwordVal !== repeatPasswordVal) {
        notification["error"]({
          message: "Los passwords no son iguales",
        });
      } else {
        const result = await signUpApi(input);

        if (result.msg) {
          notification["success"]({
            message: result.msg,
          });

          resetForm();
        } else {
          notification["error"]({
            message: result.errors[0].msg,
          });
        }
      }
    }
  };

  const resetForm = () => {
    const input = document.getElementsByTagName("input");

    for (let i = 0; i < input.length; i++) {
      input[i].classList.remove("success");
      input[i].classList.remove("error");
    }

    setInput({
      correo: "",
      password: "",
      repeatPassword: "",
      privacyPolicy: false,
    });

    setFormValid({
      correo: false,
      password: false,
      repeatPassword: false,
      privacyPolicy: false,
    });
  };

  return (
    <>
      <Form
        className="register-form"
        onSubmitCapture={register}
        onChange={changeForm}
      >
        <Form.Item>
          <Input
            prefix={<UserAddOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
            type="email"
            name="correo"
            placeholder="email"
            className="register-form__input"
            onChange={inputValidation}
            value={input.correo}
          ></Input>
        </Form.Item>

        <Form.Item>
          <Input.Password
            prefix={<LockOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
            placeholder="input password"
            name="password"
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            className="register-form__input"
            onChange={inputValidation}
            value={input.password}
          />
        </Form.Item>

        <Form.Item>
          <Input.Password
            prefix={<LockOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
            placeholder="input password"
            name="repeatPassword"
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            className="register-form__input"
            onChange={inputValidation}
            value={input.repeatPassword}
          />
        </Form.Item>

        <Form.Item>
          <Checkbox
            name="privacyPolicy"
            onChange={inputValidation}
            checked={input.privacyPolicy}
          >
            He leído y acepto la politica de privacidad
          </Checkbox>
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="register-form__button"
          >
            Crear Cuenta
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};
