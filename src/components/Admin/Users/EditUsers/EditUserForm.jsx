import React, { useCallback, useEffect, useState } from 'react'
import { Avatar, Form, Input, Select, Button, Row, Col, notification } from 'antd'
import { MailOutlined, UsergroupAddOutlined, UserOutlined } from '@ant-design/icons'
import { useDropzone } from 'react-dropzone'
import NoAvatar from '../../../../assets/images/png/no-avatar.png'
import { updateUserApi, upLoadAvatarApi, getAvatarApi } from '../../../../api/user.api'
import { customMessage } from '../../../../constants/customMessage'
import './EditUserForm.scss'

/**
 * Formulario de edicion de usuario
 * @param {props} props 
 * @returns Componenete para edicion del formulario
 */
export const EditUserForm = (props) => {

    const { user, setIsVisibleModal, setReloadUsers } = props
    const [img, setImg] = useState(null);
    const [avatar, setAvatar] = useState(null)
    const [userData, setUserData] = useState({});

    useEffect(() => {
        setUserData({
            nombre: user.nombre,
            lastname: user.lastname,
            rol: user.rol,
            img: user.img,
            uid: user.uid,
            correo: user.correo,
            avatar: user.avatar
        })
    }, [user])

    useEffect(() => {
        if (user.avatar) {
            getAvatarApi(user.uid).then(response => {
                setImg(response)
            })
        } else {
            setAvatar(null)
        }
    }, [user])

    useEffect(() => {
        if (avatar) {
            setUserData({ ...userData, avatar: avatar.file })
        }
    }, [avatar])


    const updateUser = e => {
        e.preventDefault();

        let usrUpdate = userData

        if (!usrUpdate.nombre || !usrUpdate.correo || !usrUpdate.lastname) {
            notification['error']({
                message: customMessage.REQ_NAME_LASTNAME
            })
            return
        }

        //update avatar
        if (usrUpdate.avatar) {
            upLoadAvatarApi(usrUpdate.avatar, user.uid)
                .then(() => {
                    updateUserApi(usrUpdate)
                        .then(() => {
                            notification['success']({
                                message: customMessage.UPDATE_USER
                            })
                            setReloadUsers(true)
                        })
                })
        } else {

            //update user
            updateUserApi(usrUpdate)
                .then(result => {
                    notification['success']({
                        message: customMessage.UPDATE_USER
                    })
                    setReloadUsers(true)
                })
        }
        setIsVisibleModal(false)
    }

    return (
        <div className='edit-user-form'>
            <UpLoadAvatar img={img}
                setImg={setImg}
                userData={userData}
                avatar={avatar}
                setAvatar={setAvatar}
            />
            <EditForm user={user}
                userData={userData}
                setUserData={setUserData}
                updateUser={updateUser}
            />
        </div>
    )
}

/**
 * Edicion del usuarios
 * @param {props} props 
 * @returns Formulario de edicion de usuario
 */
const EditForm = (props) => {
    const { userData, setUserData, updateUser } = props
    const { Option } = Select

    return (
        <Form className='form-edit' onSubmitCapture={updateUser}>
            <Row gutter={24}>
                <Col span={12}>
                    <Form.Item>
                        <Input
                            prefix={<UserOutlined />}
                            placeholder='Nombre'
                            value={userData.nombre}
                            onChange={e => setUserData({ ...userData, nombre: e.target.value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item>
                        <Input
                            prefix={<UsergroupAddOutlined />}
                            placeholder='Apellidos'
                            value={userData.lastname}
                            onChange={e => setUserData({ ...userData, lastname: e.target.value })}
                        />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={24}>
                <Col span={12}>
                    <Form.Item>
                        <Input
                            prefix={<MailOutlined />}
                            placeholder='email'
                            readOnly
                            value={userData.correo}
                            onChange={e => setUserData({ ...userData, correo: e.target.value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item>
                        <Select className='select-rol'
                            placeholder='Seleciona un Rol'
                            onChange={e => setUserData({ ...userData, rol: e })}
                            value={userData.rol}
                        >
                            <Option value='ADMIN_ROLE'>Administrador</Option>
                            <Option value='VENTAS_ROLE'>Ventas</Option>
                            <Option value='USER_ROLE'>Usuario Web</Option>

                        </Select>
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Button type="primary" htmlType='submit' className='btn-submit' >
                    Guardar
                </Button>
            </Form.Item>
        </Form>
    )
}


const UpLoadAvatar = (props) => {

    const { avatar, setAvatar, userData } = props
    const [avatarUrl, setAvatarUrl] = useState(null)

    useEffect(() => {
        if (avatar) {
            if (avatar.preview) {
                setAvatarUrl(avatar.preview)
            } else {
                setAvatarUrl(avatar)
            }
        } else {
            setAvatarUrl(null)
        }
    }, [avatar])

    const onDrop = useCallback(
        acceptedFiles => {
            const file = acceptedFiles[0]
            const archivo = acceptedFiles
            setAvatar({ file, preview: URL.createObjectURL(file), archivo })
        }, [setAvatar]
    );

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        accept: "image/jpeg, image/png, image/jpg",
        noKeyboard: true,
        onDrop
    });

    return (
        <div className='upload-avatar' {...getRootProps()}>
            <input {...getInputProps()} />
            {
                isDragActive ? (
                    <Avatar size={150} src={NoAvatar} />)
                    : (
                        <Avatar size={150} src={avatarUrl ? avatarUrl
                            : userData.img ? userData.img : NoAvatar} />
                    )}
        </div>
    )
}


