import React, { useState } from 'react'
import { Form, Input, Select, Row, Col, Button, notification } from 'antd'
import { MailOutlined, UsergroupAddOutlined, UserOutlined } from '@ant-design/icons'
import { insertUserApi } from '../../../../api/user.api'
import { customMessage as msj, typeStatus } from '../../../../constants/customMessage'

import './AddUser.scss'

export const AddUser = (props) => {

    const { setIsVisibleModal, setReloadUsers } = props
    const [userData, setUserData] = useState({})

    const addUser = e => {
        e.preventDefault()
        if (!userData.nombre || !userData.lastname ||
            !userData.correo || !userData.rol) {
            notification['error']({
                message: msj.ALL_FIELD_REQ
            })
        } else {
            insertUserApi(userData)
                .then(response => {
                    notification[response.type]({
                        message: response.msg
                    })
                    if (response.type === typeStatus.success) {
                        setIsVisibleModal(false)
                        setUserData({})
                        setReloadUsers(true)
                    }
                })
                .catch(err => {
                    notification['error']({
                        message: err
                    })
                })
        }
    }

    return (
        <>
            <div className='add-user-form'>
                <AddForm
                    userData={userData}
                    setUserData={setUserData}
                    addUser={addUser}
                />
            </div>
        </>
    )
}


const AddForm = (props) => {

    const { userData, setUserData, addUser } = props
    const { Option } = Select

    return (
        <Form className='form-add' onSubmitCapture={addUser}>
            <Row gutter={24}>
                <Col span={12}>
                    <Form.Item>
                        <Input
                            prefix={<UserOutlined />}
                            placeholder='Nombre'
                            value={userData.nombre}
                            onChange={e => setUserData({ ...userData, nombre: e.target.value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item>
                        <Input
                            prefix={<UsergroupAddOutlined />}
                            placeholder='Apellidos'
                            value={userData.lastname}
                            onChange={e => setUserData({ ...userData, lastname: e.target.value })}
                        />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={24}>
                <Col span={12}>
                    <Form.Item>
                        <Input
                            prefix={<MailOutlined />}
                            placeholder='email'
                            value={userData.correo}
                            onChange={e => setUserData({ ...userData, correo: e.target.value })}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item>
                        <Select className='select-rol'
                            placeholder='Seleciona un Rol'
                            onChange={e => setUserData({ ...userData, rol: e })}
                            value={userData.rol}
                        >
                            <Option value='ADMIN_ROLE'>Administrador</Option>
                            <Option value='VENTAS_ROLE'>Ventas</Option>
                            <Option value='USER_ROLE'>Usuario Web</Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Button type="primary" htmlType='submit' className='btn-submit' >
                    Guardar
                </Button>
            </Form.Item>
        </Form>
    )

}
