import React, { useState } from 'react'
import { Switch, List, Avatar, Button, notification } from 'antd'
import { CheckOutlined, EditOutlined, StopOutlined } from '@ant-design/icons'
import NoAvatar from '../../../../assets/images/png/no-avatar.png'
import { Modal } from '../../../Modal'
import { EditUserForm } from '../EditUsers/EditUserForm'
import { ativateUser } from '../../../../api/user.api'
import { customMessage as msj } from '../../../../constants/customMessage'
import { AddUser } from '../AddUser/AddUser'

import './ListUsers.scss'

export const ListUsers = (props) => {
    const { activeUsers, inactiveUsers, setReloadUsers } = props
    const [viewUserActive, setViewUserActive] = useState(true)
    const [isVisibleModal, setIsVisibleModal] = useState(false)
    const [modalTitle, setModalTitle] = useState('')
    const [modalContent, setmodalContent] = useState(null)

    const addUserModal = () => {
        setIsVisibleModal(true)
        setModalTitle('Creando nuevo usuario')
        setmodalContent(
            <AddUser setIsVisibleModal={setIsVisibleModal} setReloadUsers={setReloadUsers} />
        )
    }

    return (
        <>
            <div className='list-users'>
                <div className='list-users__header'>
                    <div className='list-users__header-switch'>
                        <Switch
                            defaultChecked
                            onChange={() => setViewUserActive(!viewUserActive)}
                        />
                        <span>
                            {viewUserActive ? 'Usuarios activos' : 'Usuarios inactivos'}
                        </span>
                    </div>
                    <Button type='primary' onClick={() => addUserModal()}>
                        Nuevo Usuario
				    </Button>
                </div>

                {
                    viewUserActive ? (
                        <ActiveUsersList
                            activeUsers={activeUsers}
                            setIsVisibleModal={setIsVisibleModal}
                            setModalTitle={setModalTitle}
                            setmodalContent={setmodalContent}
                            setReloadUsers={setReloadUsers}
                        />
                    ) : (
                        <InactiveUsersList inactiveUsers={inactiveUsers} setReloadUsers={setReloadUsers} />
                    )
                }

                <Modal
                    title={modalTitle}
                    isVisible={isVisibleModal}
                    setIsVisible={setIsVisibleModal}
                >
                    {modalContent}
                </Modal>
            </div>
        </>
    )
}


const ActiveUsersList = (props) => {
    const { usuarios } = props.activeUsers
    const { setReloadUsers } = props
    const { setIsVisibleModal, setModalTitle, setmodalContent } = props

    //Funcion para editar el usuario
    const editUser = (user) => {
        setIsVisibleModal(true)
        setModalTitle(`Usuario: ${user.correo}`)
        setmodalContent(
            <EditUserForm user={user}
                setIsVisibleModal={setIsVisibleModal}
                setReloadUsers={setReloadUsers}
            />
        )
    }

    return (
        <List
            className='user-active'
            itemLayout='horizontal'
            dataSource={usuarios}
            renderItem={user => (
                <List.Item
                    actions={
                        [
                            <Button type='primary' onClick={() => editUser(user)} >
                                <EditOutlined />
                            </Button>,
                            <Button style={{ background: "#ad2102", borderColor: "red" }} type='danger'
                                onClick={() => {
                                    activeDeactivateUser(user, false)
                                    setReloadUsers(true)
                                }} >
                                <StopOutlined />
                            </Button>
                            // <Button type='danger' onClick={() => console.log('Eliminar usuario')} >
                            //     <DeleteOutlined />
                            // </Button>
                        ]
                    }

                >
                    <List.Item.Meta
                        avatar={<Avatar src={user.img ? user.img : NoAvatar} />}
                        title={`
                        ${user.nombre ? user.nombre : '...'}
                        ${user.lastname ? user.lastname : '...'}
                        `}
                        description={user.correo}
                    />
                </List.Item >
            )}
        />
    )
}


const InactiveUsersList = (props) => {

    const { usuarios } = props.inactiveUsers
    const { setReloadUsers } = props

    return (
        <List
            className='user-active'
            itemLayout='horizontal'
            dataSource={usuarios}
            renderItem={user => (
                <List.Item
                    actions={
                        [
                            <Button type='primary' onClick={() => {
                                activeDeactivateUser(user, true)
                                setReloadUsers(true)
                            }} >
                                <CheckOutlined />
                            </Button>
                            // <Button type='danger' onClick={() => console.log('Eliminar usuario')} >
                            //     <DeleteOutlined />
                            // </Button>
                        ]
                    }

                >
                    <List.Item.Meta
                        avatar={<Avatar src={user.img ? user.img : NoAvatar} />}
                        title={`
                            ${user.nombre ? user.nombre : '...'}
                            ${user.lastname ? user.lastname : '...'}
                        `}
                        description={user.correo}
                    />
                </List.Item>
            )}
        />
    )
}


/**
 * Inactiva o desactiva un usuario. 
 * @param {User} user Objeto de usuario a desactivar
 * @param {Boolean} estado Nuevo estado True or False del Usuario
 */
const activeDeactivateUser = (user, estado = true) => {
    ativateUser(user, estado)
        .then(response => {
            if (response.ok) {
                notification['success']({ message: msj.UPDATE_USER })
            } else { notification['error']({ message: msj.DEFAULT_ERROR }) }
        })
        .catch(err => { notification['error']({ message: err }) })
}