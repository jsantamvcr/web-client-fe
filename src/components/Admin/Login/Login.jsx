import React, { useState } from 'react'
import { Form, Input, Button, notification } from 'antd'
import { LockOutlined, UserAddOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons'
import { loginApi } from '../../../api/user.api'
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../../../utils/constants'

import './Login.scss'

export const Login = () => {


    const [inputs, setInputs] = useState({
        correo: '',
        password: ''
    })

    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    const login = async (e) => {
        e.preventDefault()

        const result = await loginApi(inputs)
        if (result.token) {
            const { token, refreshToken } = result
            localStorage.setItem(ACCESS_TOKEN, token)
            localStorage.setItem(REFRESH_TOKEN, refreshToken)

            notification['success']({
                message: 'Login corecto'
            })

            window.location.href = "/admin"

        } else {
            notification['warning']({
                message: `Login Fallido ${result.msg}`
            })
        }

    }

    return (
        <>
            <Form className='login-form' onChange={changeForm} onSubmitCapture={login}>
                <Form.Item>
                    <Input
                        prefix={<UserAddOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type='email'
                        name='correo'
                        placeholder='email'
                        className='login-form__input'
                    // onChange={inputValidation}
                    // value={input.correo}
                    >
                    </Input>

                </Form.Item>
                <Form.Item>
                    <Input.Password
                        prefix={<LockOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="input password"
                        name='password'
                        iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                        className='login-form__input'
                    // onChange={inputValidation}
                    // value={input.password}
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType='submit' className='login-form__button'>
                        Login
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

