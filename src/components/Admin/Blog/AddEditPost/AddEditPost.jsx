import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Form,
  Button,
  DatePicker,
  Input,
  notification,  
} from "antd";
import {  
  FontSizeOutlined,  
  LinkOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { Editor } from "@tinymce/tinymce-react";
import {
  customMessage as msg,
  typeStatus as ts,
} from "../../../../constants/customMessage";
import { getAccessToken, addNewPostApi, updatePostApi } from "../../../../api";
import "./AddEditPost.scss";

export const AddEditPost = (props) => {
  const { setIsVisibleModal, setReloadPosts, post } = props;

  const [postData, setPostData] = useState({});

  useEffect(() => {
    if (post) {
      setPostData(post);
    } else {
      setPostData({});
    }
  }, [post]);

  //Add or Create proces post
  const processPost = (e) => {
    e.preventDefault();

    const { title, url, date, description } = postData;

    if (!title || !url || !date || !description) {
      notification[ts.error]({
        message: msg.ALL_FIELD_REQ,
      });
    } else {
      if (!post) {
        addPost();
      } else {
        updatePost()
      }
    }
  };

  const addPost = () => {
    const token = getAccessToken();

    addNewPostApi(token, postData)
      .then((res) => {
        if (res.code === 200) {
          notification[ts.success]({
            message: msg.DATA_SAVE_SUCCESS,
          });
        } else {
          notification[ts.warning]({
            message: res.errors[0].msg,
          });
        }
        setIsVisibleModal(false);
        setReloadPosts(true);
        setPostData({});
      })
      .catch((error) => {
        notification[ts.error]({
          message: msg.ERROR_SERVER,
        });
        console.error("addPost", error);
      });
  };

  const updatePost = () => {
    const token = getAccessToken();
    updatePostApi(token, post.uid, postData)
      .then((res) => {
        if (res.code === 200) {
          notification[ts.success]({
            message: msg.DATA_SAVE_SUCCESS,
          });
        } else {
          notification[ts.warning]({
            message: res.errors[0].msg,
          });
        }
        setIsVisibleModal(false);
        setReloadPosts(true);
        setPostData({});
      })
      .catch((error) => {
        notification[ts.error]({
          message: msg.ERROR_SERVER,
        });
        console.error("addPost", error);
      });
  };

  return (
    <div>
      <AddEditForm
        postData={postData}
        setPostData={setPostData}
        post={post}
        processPost={processPost}
      />
    </div>
  );
};

//New Componetn Internal
const AddEditForm = (props) => {
  const { postData, setPostData, post, processPost } = props;

  return (
    <Form
      className="add-edit-post-form"
      layout="horizontal"
      onSubmitCapture={processPost}
    >
      <Row gutter={24}>
        <Col span={8}>
          <Input
            prefix={<FontSizeOutlined />}
            placeholder="Titulo"
            value={postData.title}
            onChange={(e) =>
              setPostData({ ...postData, title: e.target.value })
            }
          />
        </Col>
        <Col span={8}>
          <Input
            prefix={<LinkOutlined />}
            placeholder="URL"
            value={postData.url}
            onChange={(e) =>
              setPostData({
                ...postData,
                url: transformTextToUrl(e.target.value),
              })
            }
          />
        </Col>
        <Col span={8}>
          <DatePicker
            style={{ width: "100%" }}
            format="DD/MM/YYYY HH:mm:ss"
            placeholder="Fecha de Publicacion"
            value={postData.date && moment(postData.date)}
            onChange={(e, value) =>
              setPostData({
                ...postData,
                date: moment(value, "DD/MM/YYYY HH:mm:ss").toISOString(),
              })
            }
            //showTime={{ defaultValue: moment("00:00:00", "HH:mm:ss") }}
          />
        </Col>
      </Row>
      <Editor
        initialValue={postData.description ? postData.description : ""}
        // value={postData.description ? postData.description : ""}
        init={{
          height: 500,
          menubar: false,
          plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste code help wordcount",
          ],
          toolbar:
            "undo redo | formatselect | " +
            "bold italic backcolor | alignleft aligncenter " +
            "alignright alignjustify | bullist numlist outdent indent | " +
            "removeformat | help",
          content_style:
            "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
        }}
        //we need lost control of Editor Componet, when click in button post
        onBlur={(e) => {
          setPostData({
            ...postData,
            description: e.target.getContent(),
          });
        }}
      />
      <Button type="primary" htmlType="submit" className="btn-submit">
        {post ? "Actualizar Post" : "Crear Post"}
      </Button>
    </Form>
  );
};

/**
 * Replace space with -
 * and transform in lower case
 * @param {String} text
 * @returns
 */
const transformTextToUrl = (text) => {
  const url = text.replace(" ", "-");
  return url.toLowerCase();
};
