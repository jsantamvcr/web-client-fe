import React from "react";
import { Button, notification, List, Modal } from "antd";
import { Link } from "react-router-dom";
import { DeleteFilled, EditFilled, EyeFilled } from "@ant-design/icons";
import { deletePostApi, getAccessToken } from "../../../../api";
import { typeStatus as ts } from "../../../../constants/customMessage";
import "./PostsLists.scss";

const { confirm } = Modal;

export const PostsLists = (props) => {
  const { posts, setReloadPosts, editPost } = props;
  const { docs } = posts;

  /** Delete Post */
  const deletePost = (post) => {
    const token = getAccessToken();

    confirm({
      title: "Eliminando Post",
      content: `¿Seguro de eliminar el post [ ${post.title} ]?`,
      okText: "Eliminar",
      okType: "danger",
      cancelText: "Cancelar",
      onOk() {
        deletePostApi(token, post.uid)
          .then((response) => {
            const typeN = response.code === 200 ? ts.success : ts.warning;
            notification[typeN]({
              message: response.msg,
            });
            setReloadPosts(true);
          })
          .catch((error) => {
            notification["error"]({
              message: "Error server",
            });
          });
      },
    });
  };

  return (
    <div className="posts-list">
      <List
        dataSource={docs}
        renderItem={(post) => (
          <Post post={post} deletePost={deletePost} editPost={editPost} />
        )}
      />
    </div>
  );
};

/**
 * Internal components List for
 * @param {*} props
 * @returns
 */
const Post = (props) => {
  const { post, deletePost, editPost } = props;
  return (
    <List.Item
      actions={[
        <Link to={`/blog/${post.url}`} target="_blank">
          <Button type="primary">
            <EyeFilled />
          </Button>
        </Link>,
        <Button type="secondary" onClick={() => editPost(post)}>
          <EditFilled />
        </Button>,
        <Button type="danger" onClick={() => deletePost(post)}>
          <DeleteFilled />
        </Button>,
      ]}
    >
      <List.Item.Meta title={post.title} />
    </List.Item>
  );
};
