export { AddEditPost } from "./Blog/AddEditPost/AddEditPost";
export { AddMenuWeb } from "./MenuWeb/AddMenuWeb/AddMenuWeb";
export { AddUser } from "./Users/AddUser/AddUser";
export { EditUserForm } from "./Users/EditUsers/EditUserForm";
export { EditMenuWeb } from "./MenuWeb/EditMenuWeb/EditMenuWeb";
export { ListUsers } from "./Users/ListUsers/ListUsers";
export { Listado as CoursesList } from "./Courses/Listado/Listado";
export { MenuWebList } from "./MenuWeb/MenuWebList/MenuWebList";
export { Login } from "./Login";
export { Register } from "./Register";
export { PostsLists } from "./Blog/PostList/PostsLists";

