import React, { createContext, useEffect, useState } from "react";
import jwtDecode from "jwt-decode";
import {
  getAccessToken,
  getRefreshAccessToken,
  refreshAccessToken,
  logout,
} from "../../api/auth.api";

export const AuthContext = createContext();

export const AuthProvider = (props) => {
  const { children } = props;

  const [user, setUser] = useState({
    user: null,
    isLoading: true,
  });

  useEffect(() => {
    checkUserLogin(setUser);
  }, []);

  return <AuthContext.Provider value={user}>{children}</AuthContext.Provider>;
};

/**
 * Validacion de un usuario
 * si esta debidamente logueado
 * @param {*} setUser
 */
const checkUserLogin = (setUser) => {
  const accesstoken = getAccessToken();

  if (!accesstoken) {
    const refreshToken = getRefreshAccessToken();
    if (!refreshToken) {
      logout();
      setUser({
        user: null,
        isLoading: false,
      });
    } else {
      refreshAccessToken(refreshToken);
    }
  } else {
    setUser({
      isLoading: false,
      user: jwtDecode(accesstoken),
    });
  }
};
