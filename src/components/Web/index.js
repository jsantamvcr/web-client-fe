export { PostInfo } from "./Blog/PostInfo/PostInfo";
export { PostsListWeb } from "./Blog/PostsListWeb/PostsListWeb";
export { CoursesList } from "./Courses/CoursesList/CoursesList";
export { HomeCourses } from "./HomeCourses/HomeCourses";
export { HowMyCoursesWork } from "./HowMyCoursesWork/HowMyCoursesWork";
export { Footer } from "./Footer/Footer";
export { MainBanner } from "./MainBanner/MainBanner";
export { MenuTop } from "./MenuTop/MenuTop";
export { Newsletter } from "./Newsletter/Newsletter";
export { PersonalInfo } from "./Footer/PersonalInfo/PersonalInfo";
export { PresentationCourses } from "./Courses/PresentationCourses/PresentationCourses";
export { ReviewsCourses } from "./ReviewsCourses/ReviewsCourses";
export { SocialLinks } from './SocialLinks/SocialLinks'



