import "./SocialLinks.scss";
import React from "react";
import { ReactComponent as FacebookIcon } from "../../../assets/images/svg/facebook.svg";
import { ReactComponent as LinkedinIcon } from "../../../assets/images/svg/linkedin.svg";
import { ReactComponent as TwitterIcon } from "../../../assets/images/svg/twitter.svg";
import { ReactComponent as YoutubeIcon } from "../../../assets/images/svg/youtube.svg";

/**
 * Links de Social Media
 * @returns Component Social Links
 */
export const SocialLinks = () => {
  return (
    <div className="social-links">
      <a
        href="https://www.youtube.com/channel/UCJvb72_3nHHjI0wBOQjKdVQ"
        className="youtube"
        target="_blank"
        rel="noopener noreferrer"
      >
        <YoutubeIcon />
      </a>
      <a
        href="https://twitter.com/jsantamv"
        className="twitter"
        target="_blank"
        rel="noopener noreferrer"
      >
        <TwitterIcon />
      </a>
      <a
        href="https://www.linkedin.com/in/jsantamv/"
        className="linkedin"
        target="_blank"
        rel="noopener noreferrer"
      >
        <LinkedinIcon />
      </a>
      <a
        href="https://www.facebook.com/jsantamv"
        className="facebook"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FacebookIcon />
      </a>
    </div>
  );
};
