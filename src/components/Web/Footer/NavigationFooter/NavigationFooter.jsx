import React from "react";
import { Row, Col } from "antd";
import {
  AppstoreAddOutlined,
  BookOutlined,
  CodeOutlined,
  DatabaseOutlined,
  HddOutlined,
  RightOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import "./NavigationFooter.scss";

export const NavigationFooter = () => {
  return (
    <Row className="navigation-footer">
      <Col md={24}>
        <h3>Site Map</h3>
      </Col>
      <Col md={12}>
        <RenderListLeft />
      </Col>
      <Col md={12}>
        <RenderListRigth />
      </Col>
    </Row>
  );
};

const RenderListLeft = () => {
  return (
    <div>
      <ul>
        <li>
          <a href="#">
            <HddOutlined /> Sistemas / Servidores
          </a>
        </li>
        <li>
          <a href="#">
            <AppstoreAddOutlined /> CMS
          </a>
        </li>
        <li>
          <Link to="/contact">
            <UserOutlined /> Porfolio
          </Link>
        </li>
        <li>
          <a href="#">
            <RightOutlined /> Politica de Cookies
          </a>
        </li>
      </ul>
    </div>
  );
};

const RenderListRigth = () => {
  return (
    <div>
      <ul>
        <li>
          <a href="#">
            <BookOutlined /> Cursos OnLine
          </a>
        </li>
        <li>
          <a href="#">
            <DatabaseOutlined /> Base de datos
          </a>
        </li>
        <li>
          <Link to="/contact">
            <CodeOutlined /> Desarrollo Web
          </Link>
        </li>
        <li>
          <a href="#">
            <RightOutlined /> Politica de Privacidad
          </a>
        </li>
      </ul>
    </div>
  );
};
