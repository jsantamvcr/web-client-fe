import React from "react";
import { Layout, Row, Col } from "antd";
import "./Footer.scss";
import { PersonalInfo } from "./PersonalInfo/PersonalInfo";
import { NavigationFooter } from "./NavigationFooter/NavigationFooter";
import { Newsletter } from "../Newsletter/Newsletter";

export const Footer = () => {
  const { Footer } = Layout;

  return (


    <Footer className="footer">
      <Row>
        <Col lg={4}></Col>
        <Col lg={16}>
          <Row>
            <Col md={8}>
              <PersonalInfo />
            </Col>
            <Col md={8}>
              <NavigationFooter></NavigationFooter>
            </Col>
            <Col md={8}>
              <Newsletter />
            </Col>
          </Row>

          <Row className="footer__copyright">
            <Col md={12}>© 2021 All rights reserved </Col>
            <Col md={12}> Juan Carlos Santamaria V. Desarrollador Web</Col>
          </Row>
        </Col>
        <Col lg={4}></Col>
      </Row>
    </Footer>
  );
};
