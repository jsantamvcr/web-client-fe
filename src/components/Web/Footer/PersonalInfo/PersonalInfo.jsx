import React from "react";
import "./PersonalInfo.scss";
import LogoWhite from '../../../../assets/images/png/logo-white.png'
import { SocialLinks } from "../../SocialLinks/SocialLinks";

/**
 * Informacion personal
 * @returns
 */
export const PersonalInfo = () => {
  return (
    <div className="my-info">
      <img src={LogoWhite} alt="Juan Carlos Santamaria Villegas" />
      <h4>Diseñar es programar el arte. El diseño genial es el arte de un desarrollo web</h4>
      <SocialLinks/>
    </div>
  );
};
