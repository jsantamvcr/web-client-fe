import React, { useState } from "react";
import { Form, Input, Button, notification } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { subscribeNewsletterApi } from "../../../api/newsletter.api";
import {
  customMessage as msg,
  typeStatus as ts,
} from "../../../constants/customMessage";
import "./Newsletter.scss";

export const Newsletter = () => {
  const [email, setEmail] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();

    if (!email) {
      notification[ts.warning]({
        message: "Email vacio",
      });

      return;
    }

    const response = subscribeNewsletterApi(email);
    response.then((res) => {
      if (!res.errors) {
        notification[ts.success]({
          message: msg.DATA_SAVE_SUCCESS,
        });
        setEmail("");
      } else {
        notification[ts.warning]({
          message: res.errors[0].msg,
        });
      }
    });
  };

  return (
    <div className="newsletter">
      <h3>Newsletter</h3>
      <Form onSubmitCapture={onSubmit}>
        <Form.Item>
          <Input
            prefix={<UserOutlined style={{ color: "rgba(0,0,0,0.25)" }} />}
            placeholder="Correo electrónico"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Item>
        <Form.Item>            
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            !Me suscribo
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
