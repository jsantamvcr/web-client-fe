import React, { useEffect, useState } from "react";
import moment from "moment";
import queryString from "query-string";
import { Spin, List, notification } from "antd";
import { Link } from "react-router-dom";
import { getPostApi } from "../../../../api";
import { Pagination } from "../../../Pagination";
import {
  typeStatus as ts,
  customMessage as msg,
} from "../../../../constants/customMessage";

import "moment/locale/es";

import "./PostsListWeb.scss";
import { Helmet } from "react-helmet";

export const PostsListWeb = (props) => {
  const { history, location } = props;
  const [posts, setPosts] = useState(null);
  const { page = 1 } = queryString.parse(location.search);

  //Use Efect de change page
  useEffect(() => {
    getPostApi(12, page)
      .then((response) => {
        if (response?.code !== 200) {
          notification[ts.warning]({
            message: response.msg,
          });
        } else {
          setPosts(response.posts);
        }
      })
      .catch(() => {
        notification[ts.error]({
          message: msg.DEFAULT_ERROR,
        });
      });
  }, [page]);

  if (!posts) {
    return (
      <Spin tip="Cargando ..." style={{ width: "100%", padding: "200px 0" }} />
    );
  }

  return (
    <>
      <Helmet>
        <title>Blog de Programacion | SYSJS </title>
      </Helmet>
      <div className="post-list-web">
        <h1>Blog</h1>
        <List
          dataSource={posts.docs}
          renderItem={(post) => <Post post={post} location={location} />}
        />
        <Pagination posts={posts} location={location} history={history} />
        <br />
      </div>
    </>
  );
};

const Post = (props) => {
  const { post } = props;
  const day = moment(post.date).format("DD");
  const month = moment(post.date).format("MMMM");

  const linkto = `blog/${post.url}`;
  return (
    <List.Item className="post">
      <div className="post__date">
        <span>{day}</span>
        <span>{month}</span>
      </div>
      <List.Item.Meta title={<Link to={linkto}>{post.title}</Link>} />
    </List.Item>
  );
};
