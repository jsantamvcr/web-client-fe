import React, { useEffect, useState } from "react";
import { Spin, notification } from "antd";
import moment from "moment";
import { getPostbyUrlApi } from "../../../../api/post.api";
import "moment/locale/es";

import "./PostInfo.scss";
import {
  customMessage as msg,
  typeStatus as ts,
} from "../../../../constants/customMessage";
import { Helmet } from "react-helmet";

export const PostInfo = (props) => {
  const { url } = props;
  const [postInfo, setPostInfo] = useState(null);

  useEffect(() => {
    getPostbyUrlApi(url)
      .then((response) => {
        if (response.code !== 200) {
          notification[ts.warning]({
            message: response.msg,
          });
        } else {
          setPostInfo(response.posts);
        }
      })
      .catch((error) => {
        notification[ts.warning]({
          message: msg.ERROR_SERVER,
        });
        console.error(error);
      });
  }, [url]);

  //Valid null value to loading data
  if (!postInfo) {
    return (
      <Spin tip={msg.LOADINDG} style={{ width: "100%", padding: "200px 0" }} />
    );
  }

  return (
    <>
      <Helmet>
        <title>
          {postInfo.title} | SYSJS
        </title>
      </Helmet>
      <div className="post-info">
        <h1 className="post-info__title">{postInfo.title}</h1>

        <div className="post-info__creation-date">
          {moment(postInfo.date).locale("es").format("LL")}
        </div>

        <div
          className="post-info__description"
          dangerouslySetInnerHTML={{ __html: postInfo.description }}
        >

        </div>
      </div>
    </>
  );
};
