import React from "react";
import { Row, Col, Card, Avatar } from "antd";
import AvatarPersona from "../../../assets/images/jpg/avatar.jpg";
import "./ReviewsCourses.scss";

export const ReviewsCourses = () => {
  return (
    <Row className="reviews-courses">
      <Row>
        {/* <Col lg={4} /> */}
        <Col lg={24} className="reviews-courses__title">
          <h2>Forma parte de los +35 mil estudiantes con mis cursos</h2>
        </Col>
        {/* <Col lg={4} /> */}
      </Row>

      <Row>
        <Col lg={4}></Col>
        <Col lg={16}>
          <Row className="row-cards">
            <Col md={8}>
              <CardReview
                name="alonso campos"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen It has survived not only five  but also the leap into electronic  remaining ess"
              />
            </Col>

            <Col md={8}>
              <CardReview
                name="Alonso campos"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen It has survived not only five  but also the leap into electronic  remaining ess"
              />
            </Col>

            <Col md={8}>
              <CardReview
                name="Juan Carlos "
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen It has survived not only five  but also the leap into electronic  remaining ess"
              />
            </Col>
          </Row>
          <Row className="row-cards">
            <Col md={8}>
              <CardReview
                name="Valentina Alex"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen It has survived not only five  but also the leap into electronic  remaining ess"
              />
            </Col>

            <Col md={8}>
              <CardReview
                name="alonso campos"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen It has survived not only five  but also the leap into electronic  remaining ess"
              />
            </Col>

            <Col md={8}>
              <CardReview
                name="alonso campos"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen It has survived not only five  but also the leap into electronic  remaining ess"
              />
            </Col>
          </Row>
        </Col>
        <Col lg={4}></Col>
      </Row>
    </Row>
  );
};

const CardReview = (props) => {
  const { name, subtitle, avatar, review } = props;
  const { Meta } = Card;

  return (
    <Card className="reviews-courses__card">
      <p>{review}</p>
      <Meta
        avatar={<Avatar src={avatar} />}
        title={name}
        description={subtitle}
      />
    </Card>
  );
};
