import React from "react";
import { Row, Col, Card } from "antd";
import {
  CheckCircleOutlined,
  ClockCircleOutlined,
  DollarOutlined,
  KeyOutlined,
  MessageOutlined,
  UserOutlined,
} from "@ant-design/icons";
import "./HowMyCoursesWork.scss";

export const HowMyCoursesWork = () => {
  return (
    <Row className="how-my-courses-work">
      <Col lg={24} className="how-my-courses-work__title">
        <h2>¿Como funcionan mis cursos?</h2>
        <h3>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's
        </h3>
      </Col>

      <Col lg={4} />
      <Col lg={16}>
        <Row className="row-cards">
          <Col md={8}>
            <CardInfo
              icon={<ClockCircleOutlined />}
              title="Cursos y clases"
              description="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical"
            ></CardInfo>
          </Col>

          <Col md={8}>
            <CardInfo
              icon={<KeyOutlined />}
              title="Acceso 24/7"
              description="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical"
            ></CardInfo>
          </Col>

          <Col md={8}>
            <CardInfo
              icon={<MessageOutlined />}
              title="Aprendizaje colaborativo"
              description="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical"
            ></CardInfo>
          </Col>
        </Row>
        {/* siguente fila */}
        <Row className="row-cards">
          <Col md={8}>
            <CardInfo
              icon={<UserOutlined />}
              title="Mejora tu perfil"
              description="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical"
            ></CardInfo>
          </Col>

          <Col md={8}>
            <CardInfo
              icon={<DollarOutlined />}
              title="Precios accesibles"
              description="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical"
            ></CardInfo>
          </Col>

          <Col md={8}>
            <CardInfo
              icon={<CheckCircleOutlined />}
              title="Certificado de finalizacion"
              description="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical"
            ></CardInfo>
          </Col>
        </Row>
      </Col>
      <Col lg={4} />
    </Row>
  );
};

const CardInfo = (props) => {
  const { icon, title, description } = props;
  const { Meta } = Card;

  return (
    <Card className="how-my-courses-work__card">
      {icon}
      <Meta title={title} description={description} />
    </Card>
  );
};
