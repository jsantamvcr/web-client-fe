import "./MenuTop.scss";
import React, { useEffect, useState } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";
import { getMenuApi } from "../../../api";
import logoWhite from "../../../assets/images/png/logo-white.png";
import { SocialLinks } from "../../Web";

/**
 * Menu top component
 * @returns MenuTop component
 */
const MenuTop = () => {
  const [menuData, setMenuData] = useState([]);

  useEffect(() => {
    getMenuApi(true, 0, 0).then((response) => {
      setMenuData(response.menus);
    });
  }, []);

  return (
    <>
      <Menu className="menu-top-web" mode="horizontal">
        <Menu.Item className="menu-top-web__logo">
          <Link to={"/"}>
            <img src={logoWhite} alt="SYSJS" />
          </Link>
        </Menu.Item>

        {menuData.map((item) => {
          const external = item.url.indexOf("http") > -1 ? true : false;

          if (external) {
            return (
              <Menu.Item key={item._id} className="menu-top-web__item">
                <a href={item.url}>{item.title}</a>
              </Menu.Item>
            );
          }
        })}
        <SocialLinks></SocialLinks>
      </Menu>
    </>
  );
};

export { MenuTop };
