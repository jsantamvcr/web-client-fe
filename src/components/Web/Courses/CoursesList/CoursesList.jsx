import React, { useEffect, useState } from "react";
import { Row, Col, Card, Button, Rate, notification } from "antd";
import { getUdemyCoursesApi } from "../../../../api";
import "./CoursesList.scss";
import { typeStatus as ts } from "../../../../constants/customMessage";

export const CoursesList = (props) => {
  const { courses } = props;

  return (
    <div className="courses-list">
      <Row>
        {courses.map((course) => (
          <Col key={course.idCourse} md={8} className="courses-list__course">
            <Course  course={course} />
          </Col>
        ))}
      </Row>
    </div>
  );
};

const Course = (props) => {
  const { Meta } = Card;
  const { course } = props;
  const [courseInfo, setCourseInfo] = useState({});
  const [urlCourse, setUrlCourse] = useState("");

  useEffect(() => {
    getUdemyCoursesApi(course.idCourse).then((response) => {
      if (response?.status !== 200) {
        notification[ts.warning]({
          message: `El curso con el id ${course.idCourse} no existe`,
        });
      } else {
        setCourseInfo(response.data);
        mountUrl(response.data.url);
      }
    });
  }, [course]);

  const mountUrl = (url) => {
    const coupon = course.coupon ? `couponCode=${course.coupon}` : "";
    if (!course.link) {
      const baseUrl = `https://www.udemy.com${url}`;
      const finalUrl = baseUrl + coupon;
      setUrlCourse(finalUrl);
    } else {
      setUrlCourse(course.link);
    }
  };

  return (
    <a href={urlCourse} target="_blank" rel="noopener noreferrer">
      <Card
        cover={<img src={courseInfo.image_480x270} alt={courseInfo.title} />}
      >
        <Meta title={courseInfo.title} description={courseInfo.headline} />
        <Button>Ingresar</Button>
        <div className="courses-list__course-footer">
          <span>{course.price ? `$${course.price}` : courseInfo.price}</span>
          <div>
            <Rate disabled defaultValue={5} />
          </div>
        </div>
      </Card>
    </a>
  );
};
