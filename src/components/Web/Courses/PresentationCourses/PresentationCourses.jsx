import React from "react";
import AcademyLogo from "../../../../assets/images/png/academy-logo.png";

import "./PresentationCourses.scss";

export const PresentationCourses = () => {
  return (
    <div className="presentation-courses">
      <img src={AcademyLogo} alt="Cursos de Juan Carlos Santamaria" />
      <p>
        Simply dummy text of the printing and typesetting industry. Lorem
        Ipsum has been the industry's standard dummy text ever since the 1500s,
        when an unknown printer took a galley of type and scrambled it to make a
        type specimen book. It has survived not only five centur
      </p>
      <p>Deles un Vistazo!!!</p>
    </div>
  );
};
