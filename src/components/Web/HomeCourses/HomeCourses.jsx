import "./HomeCourses.scss";
import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Card, Button } from "antd";
//Imagenes
import imgCssGrid from "../../../assets/images/jpg/courses/css-grid.jpg";
import imgJavaScript from "../../../assets/images/jpg/courses/javascript-es6.jpg";
import imgPrestaShop from "../../../assets/images/jpg/courses/prestashop-1-7.jpg";
import imgReactHook from "../../../assets/images/jpg/courses/react-js-hooks.jpg";
import imgReactNative from "../../../assets/images/jpg/courses/react-native.jpg";
import imgWordpress from "../../../assets/images/jpg/courses/wordpress.jpg";

export const HomeCourses = () => {
  return (
    <Row className="home-courses">
      <Col lg={24} className="home-courses__title">
        <h2>Aprende y mejora tus habilidades</h2>
      </Col>
      <Col lg={4} />
      <Col lg={16}>
     
     
     
        <Row className="row-courses">
          <Col md={6}>
            <CardCourses
              image={imgCssGrid}
              title="CSS GRID"
              subtitle="Intermedio - CSS GRID"
              link="http://localhost"
            />
          </Col>
          <Col md={6}>
            <CardCourses
              image={imgJavaScript}
              title="JavaScript"
              subtitle="Intermedio - JavaScript"
              link="http://localhost"
            />
          </Col>
          <Col md={6}>
            <CardCourses
              image={imgPrestaShop}
              title="React JS Hooks"
              subtitle="Intermedio - Prestashop"
              link="http://localhost"
            />
          </Col>
          <Col md={6}>
            <CardCourses
              image={imgReactNative}
              title="React native"
              subtitle="Intermedio - Reactnative"
              link="http://localhost"
            />
          </Col>
        </Row>
     
     
     
        <Row className="row-courses">
         
          <Col md={6}>
            <CardCourses
              image={imgWordpress}
              title="WordPress"
              subtitle="Intermedio - WordPress"
              link="http://localhost"
            />
          </Col>
          <Col md={6}></Col>
          <Col md={6}></Col>
          <Col md={6}>
            <CardCourses
              image={imgReactHook}
              title="React JS Hooks"
              subtitle="Intermedio - React/JavaScript"
              link="http://localhost"
            />
          </Col>
        </Row>
     
     
     
      </Col>
      <Col lg={4} />
      <Col lg={24} className='home-courses__more'>
          <Link to='/courses'>
              <Button>Ver más</Button>
          </Link>
      </Col>
    </Row>
  );
};

const CardCourses = (props) => {
  const { image, title, subtitle, link } = props;
  const { Meta } = Card;
  return (
    <>
      <a href={link} target="_blank" rek="noopener noreferrer">
        <Card
          className="home-courses__card"
          cover={<img src={image} alt={title} />}
          actions={[<Button>INGRESAR</Button>]}
        >
          <Meta title={title} description={subtitle} />
        </Card>
      </a>
    </>
  );
};
