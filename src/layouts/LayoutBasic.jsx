import React from "react";
import { Route, Switch } from "react-router-dom";
import { MenuTop, Footer } from "../components/Web";

import { Row, Col } from "antd";
import "./LayoutBasic.scss";

export const LayoutBasic = ({ routes }) => {
  return (
    <>
      <Row>
        <Col md={4} />
        <Col md={16}>
          <MenuTop></MenuTop>
        </Col>
        <Col md={4} />
      </Row>

      <LoadRoutes routes={routes} />
      <Footer>
      </Footer>
    </>
  );
};

const LoadRoutes = ({ routes }) => {
  return (
    <Switch>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />
      ))}
    </Switch>
  );
};
