import React, { useState } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { Layout } from 'antd'
import { MenuTop } from '../components/Admin/MenuTop'
import { MenuSider } from '../components/Admin/MenuSider'
import { SingIn } from '../pages/Admin'
import { useAuth } from '../hooks/useAuth'

import "./LayoutAdmin.scss"


export const LayoutAdmin = ({ routes }) => {
    const { Header, Content, Footer } = Layout
    const [menuCollapsed, setMenuCollapsed] = useState(true)

    const { user, isLoading } = useAuth()

   

    if (!user && !isLoading) {
        return (
            <>
                <Route path='/admin/login' component={SingIn} />
                <Redirect to='/admin/login' />
            </>
        )
    }

    if (user && !isLoading) {

        return (
            <>
                <MenuSider menuCollapsed={menuCollapsed} />

                <Layout>
                    <Layout
                        className='layout-admin'
                        style={{ marginLeft: menuCollapsed ? '80px' : '200px' }}
                    >
                        <Header className='layout-admin__header'>
                            <MenuTop menuCollapsed={menuCollapsed} setMenuCollapsed={setMenuCollapsed} />
                        </Header>

                        <Content className='layout-admin__content'>
                            <LoadRoutes routes={routes} />
                        </Content>

                        <Footer className='layout-admin__footer'>juan carlos santamaria</Footer>
                    </Layout>

                </Layout>
            </>
        )
    }

    return null

}

const LoadRoutes = ({ routes }) => {

    return (
        <Switch>
            {routes.map((route, index) => (
                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    component={route.component}
                />
            ))}
        </Switch>
    )
}
