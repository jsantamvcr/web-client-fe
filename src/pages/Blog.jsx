import React from "react";
import { Row, Col } from "antd";
import { useParams } from "react-router-dom";
import { PostInfo, PostsListWeb } from "../components/Web";

export const Blog = (props) => {
  const { url } = useParams();
  const { history, location } = props;

  return (
    <Row>
      <Col md={4} />
      <Col md={16}>
        {url ? (
          <PostInfo url={url} />
        ) : (
          <PostsListWeb history={history} location={location} />
        )}
      </Col>
      <Col md={4} />
    </Row>
  );
};
