import React from "react";
import { Helmet } from "react-helmet";
import {
  HomeCourses,
  HowMyCoursesWork,
  MainBanner,
  ReviewsCourses,
} from "../components/Web";

export const Home = () => {
  return (
    <>
      <Helmet>
        <title>Home | SYSJS</title>
        <meta name="description" content="Home | Web sobre programacion" data-react-helmet="true"/>
      </Helmet>
      <MainBanner />
      <HomeCourses />
      <HowMyCoursesWork />
      <ReviewsCourses />
    </>
  );
};
