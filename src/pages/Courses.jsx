import React, { useEffect, useState } from "react";
import { Helmet } from 'react-helmet'
import { Row, Col, Spin } from "antd";
import { CoursesList, PresentationCourses } from "../components/Web";
import { getCourses } from "../api";

export const Courses = () => {
  const [courses, setCourses] = useState(null);

  useEffect(() => {
    getCourses()
      .then((response) => {
        setCourses(response.result);
      })
      .catch(console.error());
  }, []);

  return (
    <>
      <Helmet>
        <title>Cursos | JSANTAMARIA</title>
        <meta name="description" content="Home | Web sobre programacion SYSJS" data-react-helmet="true" />
      </Helmet>
      <Row>
        <Col md={4} />
        <Col md={16}>
          <PresentationCourses />
          {!courses ? (
            <Spin
              tip="Cargando Cursos"
              style={{ textAlign: "center", width: "100%", padding: "20px" }}
            />
          ) : (
            <CoursesList courses={courses} />
          )}
        </Col>
        <Col md={4} />
      </Row>
    </>
  );
};

