export { Blog } from './Blog'
export { Courses } from './Courses'
export { Courses as AdminCourses } from './Admin'
export { Contact } from './Contact'
export { Home } from './Home'
export { Error404 } from './Error404'

