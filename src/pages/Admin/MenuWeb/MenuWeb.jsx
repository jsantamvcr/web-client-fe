import React, { useEffect, useState } from 'react'
import { getMenuApi } from '../../../api'
import { MenuWebList } from '../../../components/Admin'



export const MenuWeb = () => {

    const [menu, setMenu] = useState([])
    const [reloadMenuWeb, setReloadMenuWeb] = useState(false)

    useEffect(() => {
        getMenuApi().then(res => {
            setMenu(res.menus)
        })
        setReloadMenuWeb(false)
    }, [reloadMenuWeb])


    return (
        <div>
            <MenuWebList menu={menu} setReloadMenuWeb={setReloadMenuWeb} />
        </div>
    )
}
