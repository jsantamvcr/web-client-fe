/*Admin Pages*/
export { Admin } from './Admin'
export { Courses } from './Courses'
export { MenuWeb } from './MenuWeb/MenuWeb'
export { SingIn } from './SignIn/SignIn'
export { Users } from './Users/Users'