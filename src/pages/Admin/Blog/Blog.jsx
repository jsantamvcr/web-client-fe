import React, { useState, useEffect } from "react";
import queryString from "query-string";
import { withRouter } from "react-router-dom";
import { Button, notification } from "antd";
import { EditFilled } from "@ant-design/icons";
import { Modal } from "../../../components/Modal";
import { getPostApi } from "../../../api";
import { AddEditPost, PostsLists } from "../../../components/Admin";
import { Pagination } from "../../../components/Pagination";
import {
  customMessage as msg,
  typeStatus as ts,
} from "../../../constants/customMessage";

import "./Blog.scss";

const Blog = (props) => {
  const { location, history } = props;
  const { page = 1 } = queryString.parse(location.search);

  const [posts, setPosts] = useState(null);
  const [reloadPosts, setReloadPosts] = useState(false);
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalContent, setModalContent] = useState(null);

  useEffect(() => {
    getPostApi(12, page)
      .then((response) => {
        if (response?.code !== 200) {
          notification[ts.warning]({
            message: response.msg,
          });
        } else {
          setPosts(response.posts);
        }
      })
      .catch(() => {
        notification[ts.error]({
          message: msg.DEFAULT_ERROR,
        });
      });
    setReloadPosts(false);
  }, [page, reloadPosts]);

  const addPost = () => {
    setIsVisibleModal(true);
    setModalTitle("Creando un nuevo post");
    setModalContent(
      <AddEditPost
        setIsVisibleModal={setIsVisibleModal}
        setReloadPosts={setReloadPosts}
        post={null}
      />
    );
  };

  const editPost = (post) => {
    setIsVisibleModal(true);
    setModalTitle("Editando el post");
    setModalContent(
      <AddEditPost
        setIsVisibleModal={setIsVisibleModal}
        setReloadPosts={setReloadPosts}
        post={post}
      />
    );
  };

  if (!posts) return null;

  return (
    <div className="blog">
      <div className="blog__add-post">
        <Button type="primary" onClick={addPost}>
          <EditFilled />
          Nuevo Post
        </Button>
      </div>
      <PostsLists
        posts={posts}
        setReloadPosts={setReloadPosts}
        editPost={editPost}
      />
      <br />
      <hr />
      <Pagination posts={posts} location={location} history={history} />,
      <Modal
        title={modalTitle}
        isVisible={isVisibleModal}
        setIsVisible={setIsVisibleModal}
        width="75%"
      >
        {modalContent}
      </Modal>
    </div>
  );
};

//withRouter Help to obtain location and history page
// to use on the pagination
export default withRouter(Blog);
