import React, { useEffect, useState } from 'react'
import { getAccessToken } from '../../../api/auth.api'
import { getUserApi } from '../../../api/user.api'
import { ListUsers } from '../../../components/Admin'


import './Users.scss'


export const Users = () => {

    const [activeUsers, setActiveUsers] = useState([])
    const [inactiveUsers, setInactiveUsers] = useState([])
    const [reloadUsers, setReloadUsers] = useState(false)

    const token = getAccessToken()

    useEffect(() => {

        getUserApi(token, { estado: true })
            .then(response => { setActiveUsers(response) })

        getUserApi(token, { estado: false })
            .then(response => { setInactiveUsers(response) })

        setReloadUsers(false)

    }, [token, reloadUsers])

    return (
        <>
            <div className='users'>

                <ListUsers activeUsers={activeUsers}
                    inactiveUsers={inactiveUsers}
                    setReloadUsers={setReloadUsers}
                />
            </div>
        </>
    )
}
