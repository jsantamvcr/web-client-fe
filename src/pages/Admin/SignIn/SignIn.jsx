import React from 'react'
import { Redirect } from 'react-router-dom'
import { Layout, Tabs } from 'antd'
import { Login, Register } from '../../../components/Admin'
import { getAccessToken } from '../../../api/auth.api'

import Logo from '../../../assets/images/png/logo-white.png'

import './SignIn.scss'

export const SingIn = () => {

    const { Content } = Layout
    const { TabPane } = Tabs

    //si ya estoy logeado me lleva a admin
    //para que no entre a login
    if (getAccessToken()) {
        return <Redirect to='/admin' />
    }

    return (

        <Layout className='sign-in'>
            <Content className='sign-in__content'>
                <h1 className='sign-in__content-logo'>
                    <img src={Logo} alt="Chismenadologo" />
                </h1>
                <div className='sign-in__content-tabs'>
                    <Tabs type='card'>

                        <TabPane tab={<span>Entrar</span>} key='1'>
                            <Login />
                        </TabPane>

                        <TabPane tab={<span>Nuevo Usuario</span>} key='2'>
                            <Register />
                        </TabPane>
                    </Tabs>
                </div>
            </Content>
        </Layout>
    )
}
