import React, { useEffect, useState } from "react";
import { getCourses } from "../../api";
import { CoursesList } from "../../components/Admin";

export const Courses = () => {
  const [courses, setCourses] = useState([]);
  const [reloadCourses, setReloadCourses] = useState(false);

  useEffect(() => {
    getCourses().then((response) => {
      setCourses(response.result);
    });
    setReloadCourses(false);
  }, [reloadCourses]);

  return (
    <div className="courses">
      <CoursesList courses={courses} setReloadCourses={setReloadCourses} />
    </div>
  );
};
