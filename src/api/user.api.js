import { basePath } from './config.api'

const signUpApi = async (data) => {

    const url = `${basePath}/users/sign-up`
    const params = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    return await fetch(url, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(err => { return { msg: err, } })
}

const loginApi = async (data) => {

    const url = `${basePath}/auth/login`
    const params = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    return await fetch(url, params).then(response => {
        return response.json()
    }).then(result => {
        return result
    }).catch(err => {
        console.log(err)
        return {
            msg: err,
        }
    })
}

const getUserApi = (token, filter = {}) => {

    const { limite = 5, desde = 0, estado = true } = filter

    const url = `${basePath}/users?estado=${estado}&limite=${limite}&desde=${desde}`

    const myHeaders = new Headers();
    myHeaders.append("x-token", token);

    const params = {
        method: 'GET',
        headers: myHeaders,
    };

    return fetch(url, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(error => {
            return {
                msg: error
            }
        });
}

/**
 * Sube la imagen del avatar
 * @param {Object} avatar objeto del avatar
 * @param {String} uid id del usuario
 * @returns 
 */
const upLoadAvatarApi = async (avatar, uid) => {

    const url = `${basePath}/uploads/avatar/usuarios/${uid}`

    const formdata = new FormData();
    formdata.append("archivo", avatar, avatar.name);

    const params = {
        method: 'PUT',
        body: formdata
    };

    return fetch(url, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(error => { return error })
}

const updateUserApi = async (user) => {

    const { img, avatar, ...usuario } = user
    const url = `${basePath}/users/${user.uid}`

    const params = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(usuario)
    };

    return fetch(url, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(error => { return error });
}

/**
 * Obtiene la url de la imagen
 * @param {String} uid Identificador del usuario
 */
const getAvatarApi = (uid) => {

    const url = `${basePath}/uploads/usuarios/${uid}`
    var formdata = new FormData();

    var params = {
        method: 'GET',
        body: formdata,
    };

    return fetch(url, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(error => { return error });
}


const ativateUser = async (user, estado) => {
    const url = `${basePath}/users/${user.uid}`
    const { rol } = user
    const newState = JSON.stringify({
        'estado': estado,
        'rol': rol
    })
    const params = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: newState
    }
    return fetch(url, params)
        .then(response => { return response })
        .then(result => { return result })
        .catch(error => { return error });
}

const insertUserApi = async (user) => {

    const { img, avatar, ...usuario } = user
    const url = `${basePath}/users`
    user.password = '123123' //corregir esta seguridad

    const params = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(usuario)
    };

    const response = await fetch(url, params)
    const result = await response.json()

    if (response.ok) {
        return {
            type: 'success',
            msg: result.msg
        }
    } else {
        console.log('error', result)
        return {
            type: 'error',
            msg: result.errors[0].msg
        }
    }
}


export {
    ativateUser,
    getUserApi,
    getAvatarApi,
    insertUserApi,
    loginApi,
    signUpApi,
    upLoadAvatarApi,
    updateUserApi
}