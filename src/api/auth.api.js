import jwtDecode from 'jwt-decode'
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../utils/constants'
import { basePath } from './config.api'

/**
 * Obtiene un token
 * @returns Token
 */
export const getAccessToken = () => {
    const accesToken = localStorage.getItem(ACCESS_TOKEN)

    if (!accesToken || accesToken === 'null') {
        return null
    }

    return willExpireToken(accesToken) ? null : accesToken
}


export const getRefreshAccessToken = () => {
    const refreshToken = localStorage.getItem(REFRESH_TOKEN)

    if (!refreshToken || refreshToken === 'null') {
        return null
    }

    return willExpireToken(refreshToken) ? null : refreshToken
}


export const refreshAccessToken = async (refreshToken) => {

    const url = `${basePath}/auth/refreshToken`
    const body = {
        refreshToken
    }

    const params = {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { 'Content-Type': 'application/json' }
    }

    return await fetch(url, params)
        .then(response => {
            if (response.status !== 200) {
                return null
            }
            return response.json()
        })
        .then(result => {
            if (!result) {
                logout()
            } else {
                const { token, refreshToken } = result
                localStorage.setItem(ACCESS_TOKEN, token)
                localStorage.setItem(REFRESH_TOKEN, refreshToken)
            }
        })
}

export const logout = () => {
    localStorage.removeItem(ACCESS_TOKEN)
    localStorage.removeItem(REFRESH_TOKEN)
}


const willExpireToken = (token) => {
    const seconds = 60
    const metaToken = jwtDecode(token)

    const { exp } = metaToken
    const now = (Date.now() + seconds) / 1000

    return now > exp
}