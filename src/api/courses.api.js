import { basePath } from "./config.api";

/**
 * Agrega un nuevo curso
 * @param {String} token
 * @param {Objetct} course
 * @returns
 */
const addCourse = (token, course) => {
    const url = `${basePath}/course/`;

    const headers = new Headers();
    headers.append("x-token", token);
    headers.append("Content-Type", "application/json");

    const params = {
        method: "POST",
        headers,
        body: JSON.stringify(course),
        redirect: "follow",
    };

    return fetch(url, params)
        .then(async (response) => {
            return {
                status: response.status,
                data: await response.json()
            }
        })
        .then((result) => {
            return result;
        })
        .catch((err) => {
            return err;
        });
};

/**
 * Obtiene un listado ordenado de los cursos
 * @param {String} token
 * @returns
 */
const getCourses = () => {
    const url = `${basePath}/course?estado=1&limite=0&desde=0`;    

    const params = {
        method: "GET",
        redirect: "follow",
    };

    return fetch(url, params)
        .then((response) => {
            return response.json();
        })
        .then((result) => {
            return result;
        })
        .catch((err) => {
            return err;
        });
};

/**Actualiza un curso */
const updateCourse = async (token, data) => {

    const { uid, ...course } = data
    const url = `${basePath}/course/${uid}`

    const headers = new Headers();
    headers.append("x-token", token);
    headers.append("Content-Type", "application/json");

    const params = {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(course),
        redirect: 'follow'
    };

    return fetch(url, params)
        .then(async (response) => {
            return {
                status: response.status,
                data: await response.json()
            }
        })
        .then((result) => {
            return result;
        })
        .catch((err) => { return err; });
}

/**
 * Elmina un curso logicamente
 * @param {String} token
 * @param {*} uid
 * @returns El curso eliminado
 */
const deleteCourseApi = async (token, uid) => {
    const url = `${basePath}/course/${uid}`;

    const headers = new Headers();
    headers.append("x-token", token);
    headers.append("Content-Type", "application/json");

    const params = {
        method: "DELETE",
        headers: headers,
        body: "",
        redirect: "follow",
    };

    return fetch(url, params)
        .then(async (response) => {
            return {
                status: response.status,
                data: await response.json()
            }
        })
        .then((result) => { return result; })
        .catch((err) => { return err; });
};

/**
 * Obtiene un curso del Api Externo 
 * de udemy publico
 * @param {Number} id 
 * @returns un curso de udemy
 */
const getUdemyCoursesApi = async (id) => {
    const baseUrl = `https://www.udemy.com/api-2.0/courses/${id}/`;
    const courseParams = "?fields[course]=title,headline,price,image_480x270,url";
    const url = baseUrl + courseParams;

    const params = {
        method: "GET",
        redirect: "follow",
    };

    return fetch(url, params)
        .then(async response => {
            return {
                status: response.status,
                data: await response.json()
            };
        })
        .then((result) => { return result; })
        .catch((err) => { return err; });
};

export {
    addCourse,
    getCourses,
    updateCourse,
    getUdemyCoursesApi,
    deleteCourseApi,
};
