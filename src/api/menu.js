import { basePath } from "./config.api";

/**
 * metodo para agregar un
 * menu
 * @param {String} token
 * @param {Menu} menu
 * @returns
 */
const addMenuApi = (token, menu) => {
    const url = `${basePath}/menu/add-menu`;

    const params = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "x-token": token,
        },
        body: JSON.stringify(menu),
    };

    return fetch(url, params)
        .then((response) => {
            return response.json();
        })
        .then((result) => result.msg)
        .catch((error) => {
            console.log(error);
        });
};


/**
 * Retorna el menu ordenado
 * @param {Boolean} active 
 * @param {Integer} limite 
 * @param {Integer} desde 
 * @returns Menu
 */
const getMenuApi = async (active, limite = 0, desde = 0) => {

    const url_active = active ? `active=${active}&` : ''
    const url = `${basePath}/menu/get-menu?${url_active}limite=${limite}&desde=${desde}`;

    const params = {
        method: "GET",
        redirect: "follow",
    };

    return fetch(url, params)
        .then((response) => {
            return response.json();
        })
        .then((result) => {
            return result;
        })
        .catch((error) => {
            return error;
        });
};

/**Actualiza los datos del menu  */
const updateMenuApi = async (token, id, data) => {
    const url = `${basePath}/menu/${id}`;

    const headers = new Headers();
    headers.append("x-token", token);
    headers.append("Content-Type", "application/json");

    const requestOptions = {
        method: "PUT",
        headers: headers,
        body: JSON.stringify(data),
        redirect: "follow",
    };

    try {
        const response = await fetch(url, requestOptions);
        const result = await response.json();
        return result;
    } catch (error) {
        console.log(error);
    }
};

const deleteMenuApi = async (token, id) => {
    const url = `${basePath}/menu/${id}`;
    const headers = new Headers();
    headers.append("x-token", token);
    headers.append("Content-Type", "application/json");

    var params = {
        method: "DELETE",
        headers: headers,
        redirect: "follow",
    };

    return fetch(url, params)
        .then((response) => {
            return response.json();
        })
        .then((result) => {
            return result;
        })
        .catch((error) => {
            console.log("error", error);
            throw new error();
        });
};

export { addMenuApi, getMenuApi, updateMenuApi, deleteMenuApi };
