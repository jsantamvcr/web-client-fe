import { basePath } from './config.api'

const getPostApi = (limit, page) => {
    const url = `${basePath}/post?page=${page}&limit=${limit}`

    var params = {
        method: 'GET',
        redirect: 'follow'
    };

    return fetch(url, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(err => { return err })
}

/**
 * Delete posts
 * @param {String} token 
 * @param {Number} id 
 * @returns post was deleted
 */
const deletePostApi = (token, id) => {
    const url = `${basePath}/post/${id}`
    const myHeaders = new Headers();
    myHeaders.append("x-token", token);

    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    return fetch(url, requestOptions)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(err => { return err })
}

/**
 * Add new Post
 * @param {String} token 
 * @param {*} post 
 * @returns 
 */
const addNewPostApi = (token, post) => {
    const url = `${basePath}/post/`
    const myHeaders = new Headers();
    myHeaders.append("x-token", token);
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(post);

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    return fetch(url, requestOptions)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(error => { return error });
}

/**
 * Update exist post
 * @param {String} token 
 * @param {String} id 
 * @param {*} post 
 * @returns 
 */
const updatePostApi = (token, id, post) => {
    const urlApi = `${basePath}/post/${id}`

    const { uid, ...data } = post
    const myHeaders = new Headers();
    myHeaders.append("x-token", token);
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(data);

    const requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    return fetch(urlApi, requestOptions)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(error => { return error });
}

const getPostbyUrlApi = (url) => {
    const urlApi = `${basePath}/post/getpost/?url=${url}`
    const params = {
        method: 'GET',
        redirect: 'follow'
    };
    return fetch(urlApi, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(error => { return error });
}

export {
    getPostApi,
    deletePostApi,
    addNewPostApi,
    updatePostApi,
    getPostbyUrlApi
}
