/**
 * Modo centralizado para realizar la exportaciones
 * y que no se vean sobrecargados los imports los componentes
 */
import { getAccessToken } from "./auth.api";
import {
    getMenuApi,
    addMenuApi,
    updateMenuApi
} from "./menu";
import {
    addCourse,
    deleteCourseApi,
    getCourses,
    updateCourse,
    getUdemyCoursesApi,
} from "./courses.api";

import {
    getPostApi,
    deletePostApi,
    addNewPostApi,
    updatePostApi,
    getPostbyUrlApi
} from './post.api'

export {
    getAccessToken,
    addCourse, deleteCourseApi, getCourses, getUdemyCoursesApi, updateCourse,
    updateMenuApi, getMenuApi, addMenuApi,
    getPostApi, deletePostApi, addNewPostApi, updatePostApi, getPostbyUrlApi
};
