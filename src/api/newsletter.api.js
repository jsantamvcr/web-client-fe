import { basePath } from './config.api'

/**
 * Subscribe un cliente al correo. 
 * @param {String} email 
 */
const subscribeNewsletterApi = (email) => {

    const url = `${basePath}/newslatter/${email}`

    const params = {
        method: 'POST',
        redirect: 'follow'
    };

    return fetch(url, params)
        .then(response => { return response.json() })
        .then(result => { return result })
        .catch(err => { return err })

}

export {
    subscribeNewsletterApi
}